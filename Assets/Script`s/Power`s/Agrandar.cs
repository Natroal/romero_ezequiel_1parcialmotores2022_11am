using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agrandar : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            other.gameObject.GetComponent<JugadorController>().transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
            Destroy(gameObject);

        }
    }
}

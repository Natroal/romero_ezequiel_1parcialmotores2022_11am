using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguidorMovimiento : MonoBehaviour
{
    public float rangoDeAlerta;
    public float jumpForce = 2f;
    public LayerMask capaDelJugador;
    public Transform Jugador;
    public float velocidad;
    bool estarAlerta;
    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        estarAlerta = Physics.CheckSphere(transform.position, rangoDeAlerta, capaDelJugador);

        if (estarAlerta == true)
        {

            Vector3 posJugador = new Vector3(Jugador.position.x, Jugador.position.y, Jugador.position.z);
            transform.LookAt(posJugador);

            transform.position = Vector3.MoveTowards(transform.position, posJugador, velocidad * Time.deltaTime);

        }
      

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Player"))
        {

            if (this.CompareTag("Pum"))
            {
                collision.gameObject.GetComponent<Rigidbody>().velocity = (Vector3.down * jumpForce);
            }

         
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, rangoDeAlerta);
    }
}

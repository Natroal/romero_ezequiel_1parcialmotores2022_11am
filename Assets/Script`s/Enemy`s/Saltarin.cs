using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saltarin : MonoBehaviour
{
    public float jumpForce = 2f;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Player"))
        {

            if (this.CompareTag("Right"))
            {
                collision.gameObject.GetComponent<Rigidbody>().velocity = (Vector3.right * jumpForce);
            }

            if (this.CompareTag("Left"))
            {
                collision.gameObject.GetComponent<Rigidbody>().velocity = (Vector3.left * jumpForce);
            }
        }
    }
}

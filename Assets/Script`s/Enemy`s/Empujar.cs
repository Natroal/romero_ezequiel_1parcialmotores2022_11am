using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empujar : MonoBehaviour
{

    public GameObject[] waypoint;
    public float empujarVelocidad = 2;
    private int waypointIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MoveEmpujar();
    }

    void MoveEmpujar()
    {
        if(Vector3.Distance(transform.position,waypoint[waypointIndex].transform.position) < 0.1f)
        {
            waypointIndex++;

            if (waypointIndex >= waypoint.Length)
            {
                waypointIndex = 0;
            }
        }

        transform.position = Vector3.MoveTowards(transform.position, waypoint[waypointIndex].transform.position, empujarVelocidad * Time.deltaTime);
    }
}

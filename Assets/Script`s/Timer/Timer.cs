using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    bool active;
    public GameObject panel;
    public GameObject panelTimer;
    public GameObject panelCoin;

    public float timer = 0;
    

    public TextMeshProUGUI textoTimerPro;

    private void Start()
    {
        Time.timeScale = 1f;
        panel.SetActive(false);
        panelTimer.SetActive(true);
        panelCoin.SetActive(true);
    }

    void Update()
    {
        timer -= Time.deltaTime;

        textoTimerPro.text = "Time: " + "" + timer.ToString("f0");

        if (timer <= 0)
        {
            active = !active;
            panel.SetActive(true);
            panelTimer.SetActive(false);
            panelCoin.SetActive(false);
            Time.timeScale =  0f;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;

        }
    }

    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour
{
    

    public static int theScore;

    public TextMeshProUGUI textoCoinPro;

    // Start is called before the first frame update
    void Start()
    {
        theScore = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        

        
        textoCoinPro.text = "Score: " + theScore + "/5";

        if (theScore >= 5)
        {

            SceneManager.LoadScene("Winner");

        }

    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraController : MonoBehaviour
{
    public Vector3 offset;
    public Transform playerBody;
    [Range(0, 1)] public float lerpValue;
    public float sensibilidad;

    void Start()
    {
        //playerBody = GameObject.Find("Player").transform;
    }


    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, playerBody.position + offset, lerpValue);
        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * sensibilidad, Vector3.up) * offset;

        transform.LookAt(playerBody);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class Rotator : MonoBehaviour
{

   

    private void Start()
    {
        
    }

    void Update() 
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            
            Score.theScore += 1;
            Destroy(gameObject);
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class JugadorController : MonoBehaviour
{

    private Rigidbody rb; 
    public int rapidez;

    public float jumpHeight = 5;
    public bool grounded;

    public int maxjumpCount = 2;
    public int JumpsRemaining = 0;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        float movimientoHorizontal = Input.GetAxisRaw("Horizontal"); 
        float movimientoVertical = Input.GetAxisRaw("Vertical"); 
        
        
        Vector3 vectorMovimiento = new Vector3(movimientoHorizontal, 0.0f, movimientoVertical); 
        rb.AddForce(vectorMovimiento * rapidez);

        if (Input.GetKeyDown(KeyCode.Space) && (JumpsRemaining > 0))
        {
            rb.AddForce(Vector3.up * jumpHeight, ForceMode.Impulse);
            JumpsRemaining -= 1;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            
            SceneManager.LoadScene("Main");

        }

    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Floor")
        {
            grounded = true;
            JumpsRemaining = maxjumpCount;
        }

        if (collision.gameObject.tag == "DeadFloor")
        {
            SceneManager.LoadScene("Main");
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "Floor")
        {
            grounded = false;
        }
    }
}
